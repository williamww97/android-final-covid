package com.covid.covidfinal;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CountryViewHolder extends RecyclerView.ViewHolder {
    TextView tv_country;
    TextView tv_continent;


    public CountryViewHolder(@NonNull View itemView) {
        super(itemView);

        this.tv_country = itemView.findViewById(R.id.t_country);
        this.tv_continent = itemView.findViewById(R.id.t_continent);
    }
}

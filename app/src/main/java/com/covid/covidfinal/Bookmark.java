package com.covid.covidfinal;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "bookmark")
public class Bookmark {
    @PrimaryKey(autoGenerate = true)
    private int key;
    @ColumnInfo(name = "country")
    private String countryName;
    @ColumnInfo(name = "continent")
    private String continent;
    @ColumnInfo(name = "cases")
    private String cases;
    @ColumnInfo(name = "todaycases")
    private String todayCases;
    @ColumnInfo(name = "deaths")
    private String deaths;
    @ColumnInfo(name = "todaydeaths")
    private String todayDeaths;
    @ColumnInfo(name = "recovered")
    private String recovered;
    @ColumnInfo(name = "todayrecovered")
    private String todayRecovered;

    public Bookmark(String countryName, String continent, String cases, String todayCases, String deaths, String todayDeaths, String recovered, String todayRecovered) {
        this.countryName = countryName;
        this.continent = continent;
        this.cases = cases;
        this.todayCases = todayCases;
        this.deaths = deaths;
        this.todayDeaths = todayDeaths;
        this.recovered = recovered;
        this.todayRecovered = todayRecovered;
    }

    public Bookmark(){

    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCases() {
        return cases;
    }

    public void setCases(String cases) {
        this.cases = cases;
    }

    public String getTodayCases() {
        return todayCases;
    }

    public void setTodayCases(String todayCases) {
        this.todayCases = todayCases;
    }

    public String getDeaths() {
        return deaths;
    }

    public void setDeaths(String deaths) {
        this.deaths = deaths;
    }

    public String getTodayDeaths() {
        return todayDeaths;
    }

    public void setTodayDeaths(String todayDeaths) {
        this.todayDeaths = todayDeaths;
    }

    public String getRecovered() {
        return recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

    public String getTodayRecovered() {
        return todayRecovered;
    }

    public void setTodayRecovered(String todayRecovered) {
        this.todayRecovered = todayRecovered;
    }
}

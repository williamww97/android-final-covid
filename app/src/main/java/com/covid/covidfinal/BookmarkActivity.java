package com.covid.covidfinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookmarkActivity extends AppCompatActivity implements BookmarkAdapter.CallbackAdapter  {
    LinearLayout linearLayout;
    RecyclerView recyclerView;
    BookmarkAdapter bookmarkAdapter;
    CardView cv;
    ArrayList<Bookmark> filteredList = new ArrayList<>();
    Bookmark bookmark;
    static ArrayList<Bookmark> bookmarks = new ArrayList<>();
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);


        cv = findViewById(R.id.bookmark_card);
        linearLayout = findViewById(R.id.bookmark_activity);
        recyclerView = findViewById(R.id.recyclerview2);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //accountAdapter = new AccountAdapter(this, accountList(),this);
        bookmarkAdapter = new BookmarkAdapter(this,this);
        bookmarkAdapter.setDataAdapter(bookmarkList());
        recyclerView.setAdapter(bookmarkAdapter);
    }

    public ArrayList<Bookmark> bookmarkList(){
        bookmarks = (ArrayList<Bookmark>) BookmarkDatabase.getDatabase(getApplicationContext()).getDao().getAllBookmark();
        /*
        for(int i = 0 ; i < bookmarks.size();i++){
            Log.e("tab", "" + bookmarks.get(i).getCountryName());
        }
         */
        return bookmarks;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu2,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            SearchView searchView = (SearchView) item.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filteredList = new ArrayList<>();
                    Log.e("tab", bookmarks.size()+"");
                    for(int i = 0 ; i < bookmarks.size() ; i++){
                        if(bookmarks.get(i).getCountryName() != null){
                            if(bookmarks.get(i).getCountryName().toLowerCase().contains(newText)){
                                filteredList.add(bookmarks.get(i));
                            }
                        }
                    }
                    if(filteredList.isEmpty()){
                        showSnackBar();
                    }
                    bookmarkAdapter.setDataAdapter(filteredList);
                    bookmarkAdapter.notifyDataSetChanged();
                    return false;
                }
            });
        }
        else{
            startActivity(new Intent(this, BookmarkActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }



    public void showSnackBar(Country country){
        Snackbar snackbar = Snackbar.make(linearLayout ,country.getCountryName() + " " + country.getContinent(),Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    public void showSnackBar(){
        Snackbar snackbar = Snackbar.make(linearLayout ,"Account does not exist",Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    public void onClick(Bookmark bookmark) {
        //showSnackBar(country);
        //startActivity(new Intent(this, DetailActivity.class));
        Intent myIntent = new Intent(this, BookmarkDetailActivity.class);
        Gson gson = new Gson();
        String countryJSON = gson.toJson(bookmark);
        myIntent.putExtra("countryDetail",countryJSON);
        myIntent.putExtra("name",bookmark.getCountryName());
        startActivity(myIntent);
    }
}
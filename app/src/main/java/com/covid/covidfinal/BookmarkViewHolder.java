package com.covid.covidfinal;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BookmarkViewHolder extends RecyclerView.ViewHolder {
    TextView tv_country2;
    TextView tv_continent2;


    public BookmarkViewHolder(@NonNull View itemView) {
        super(itemView);

        this.tv_country2 = itemView.findViewById(R.id.t_country2);
        this.tv_continent2 = itemView.findViewById(R.id.t_continent2);
    }
}

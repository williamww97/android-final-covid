package com.covid.covidfinal;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Country {
    @SerializedName("country")
    private String countryName;
    @SerializedName("continent")
    private String continent;
    @SerializedName("cases")
    private String cases;
    @SerializedName("todayCases")
    private String todayCases;
    @SerializedName("deaths")
    private String deaths;
    @SerializedName("todayDeaths")
    private String todayDeaths;
    @SerializedName("recovered")
    private String recovered;
    @SerializedName("todayRecovered")
    private String todayRecovered;

    public Country(String countryName, String continent, String cases, String todayCases, String deaths, String todayDeaths, String recovered, String todayRecovered) {
        this.countryName = countryName;
        this.continent = continent;
        this.cases = cases;
        this.todayCases = todayCases;
        this.deaths = deaths;
        this.todayDeaths = todayDeaths;
        this.recovered = recovered;
        this.todayRecovered = todayRecovered;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCases() {
        return cases;
    }

    public void setCases(String cases) {
        this.cases = cases;
    }

    public String getTodayCases() {
        return todayCases;
    }

    public void setTodayCases(String todayCases) {
        this.todayCases = todayCases;
    }

    public String getDeaths() {
        return deaths;
    }

    public void setDeaths(String deaths) {
        this.deaths = deaths;
    }

    public String getTodayDeaths() {
        return todayDeaths;
    }

    public void setTodayDeaths(String todayDeaths) {
        this.todayDeaths = todayDeaths;
    }

    public String getRecovered() {
        return recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

    public String getTodayRecovered() {
        return todayRecovered;
    }

    public void setTodayRecovered(String todayRecovered) {
        this.todayRecovered = todayRecovered;
    }
}

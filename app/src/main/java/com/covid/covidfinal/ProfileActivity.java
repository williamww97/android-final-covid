package com.covid.covidfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    TextView tvname;
    TextView tvuname;
    TextView tvemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        tvname = findViewById(R.id.name);
        tvuname = findViewById(R.id.uname);
        tvemail = findViewById(R.id.email);

        tvname.setText(MainActivity.profileResponse.getProfile().getFullname());
        tvemail.setText(MainActivity.profileResponse.getProfile().getEmail());
        tvuname.setText(MainActivity.profileResponse.getProfile().getUsername());
    }
}
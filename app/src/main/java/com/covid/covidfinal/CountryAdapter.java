package com.covid.covidfinal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

public class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> implements Filterable {

    public interface CallbackAdapter{
        void onClick(Country country);
    }

    public CallbackAdapter callbackAdapter;

    Context c;
    ArrayList<Country> countries;
    ArrayList<Country> countriesFilter;

    public void setDataAdapter(ArrayList<Country> lcountry){
        this.countries = lcountry;
    }

    public CountryAdapter(Context c, CallbackAdapter callbackAdapter) {

        this.c = c;
        //this.accounts = accounts;
        //this.accountsFilter = new ArrayList();
        //accountsFilter.addAll(accounts);
        this.callbackAdapter = callbackAdapter;
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_menu_body, parent, false);

        return new CountryViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {
        holder.tv_country.setText(countries.get(position).getCountryName());
        holder.tv_continent.setText(countries.get(position).getContinent());
        holder.tv_continent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackAdapter.onClick(countries.get(position));
            }
        });
    }



    @Override
    public int getItemCount() {
        return countries.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Country> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(countriesFilter);
            } else {
                for(int i = 0 ; i < countriesFilter.size();i++){
                    if(countriesFilter.get(i).getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(countriesFilter.get(i));
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            countriesFilter.clear();
            countriesFilter.addAll((Collection<? extends Country>) results.values);
            notifyDataSetChanged();
        }
    };
}

package com.covid.covidfinal;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface InterfaceAPI {

    @POST("login")
    @FormUrlEncoded
    Call<ProfileResponse> getLoginInfo (@Header("X-API-KEY") String key, @FieldMap Map<String, String> loginmap);

    @GET
    Call<Country[]> getListCase(@Url String url);
}

package com.covid.covidfinal;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface BookmarkDao {
    @Insert
    void insertAllData(Bookmark bookmark);

    @Query("SELECT * FROM bookmark")
    List<Bookmark> getAllBookmark();

    @Query("DELETE FROM BOOKMARK")
    void dropAllData();

    @Query("DELETE FROM BOOKMARK WHERE country = :cname")
    void deleteBookmark(String cname);
}

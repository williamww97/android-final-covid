package com.covid.covidfinal;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Bookmark.class}, version = 1)
public abstract class BookmarkDatabase extends RoomDatabase {
    public abstract BookmarkDao getDao();

    private static BookmarkDatabase instance;

    static BookmarkDatabase getDatabase(final Context context){
        if(instance == null){
            synchronized (BookmarkDatabase.class){
                instance = Room.databaseBuilder(context, BookmarkDatabase.class, "DATABASE").allowMainThreadQueries().build();
            }
        }
        return instance;
    }
}

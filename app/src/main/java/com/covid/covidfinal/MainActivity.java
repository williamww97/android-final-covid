package com.covid.covidfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private SharedPreferenceConfig sharedPreferenceConfig;
    static ProfileResponse profileResponse;
    EditText username;
    EditText password;
    int code = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.e_username);
        password = findViewById(R.id.e_password);
        sharedPreferenceConfig  = new SharedPreferenceConfig(getApplicationContext());

        if(sharedPreferenceConfig.read_login_status()){
            startActivity(new Intent(this, MenuActivity.class));
            finish();
        }
    }

    public void Login(View view) {

        String uname = username.getText().toString();
        String pass = password.getText().toString();
        Map<String, String> loginMap = new HashMap<String, String>();
        loginMap.put("username", uname);
        loginMap.put("password", pass);

        InterfaceAPI interfaceAPI = RetrofitInstance.getRetrofitInstance().create(InterfaceAPI.class);
        Call<ProfileResponse> call = interfaceAPI.getLoginInfo("454041184B0240FBA3AACD15A1F7A8BB", loginMap);
        Log.e("gab", uname);
        Log.e("gab", pass);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                Log.e("dab", "" + response.code());
                Log.e("dab", "" + response.body().toString());
                code = response.code();
                profileResponse = response.body();
                Log.e("dab", "" + profileResponse.getMessage());
                Log.e("dab", "" + profileResponse.getStatus());
                Log.e("dab", "" + profileResponse.getProfile().getFullname());
                Log.e("dab", "" + profileResponse.getProfile().getEmail());

                if(code == 200){
                    startActivity(new Intent(MainActivity.this, MenuActivity.class));
                    sharedPreferenceConfig.login_status(true);
                    finish();
                }
                else{
                    Toast.makeText(MainActivity.this, "Invalid Credential!", Toast.LENGTH_SHORT).show();
                    username.setText("");
                    password.setText("");
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Log.e("tab", "failed");
            }
        });
    }
}
package com.covid.covidfinal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkViewHolder> implements Filterable {

    public interface CallbackAdapter{
        void onClick(Bookmark bookmark);
    }

    public CallbackAdapter callbackAdapter;

    Context c;
    ArrayList<Bookmark> bookmarks;
    ArrayList<Bookmark> bookmarksFilter;

    public void setDataAdapter(ArrayList<Bookmark> lbookmark){
        this.bookmarks = lbookmark;
    }

    public BookmarkAdapter(Context c, CallbackAdapter callbackAdapter) {

        this.c = c;
        //this.accounts = accounts;
        //this.accountsFilter = new ArrayList();
        //accountsFilter.addAll(accounts);
        this.callbackAdapter = callbackAdapter;
    }

    @NonNull
    @Override
    public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_bookmark_body, parent, false);

        return new BookmarkViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull BookmarkViewHolder holder, int position) {
        holder.tv_country2.setText(bookmarks.get(position).getCountryName());
        holder.tv_continent2.setText(bookmarks.get(position).getContinent());
        holder.tv_continent2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackAdapter.onClick(bookmarks.get(position));
            }
        });
    }



    @Override
    public int getItemCount() {
        return bookmarks.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Bookmark> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(bookmarksFilter);
            } else {
                for(int i = 0 ; i < bookmarksFilter.size();i++){
                    if(bookmarksFilter.get(i).getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(bookmarksFilter.get(i));
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            bookmarksFilter.clear();
            bookmarksFilter.addAll((Collection<? extends Bookmark>) results.values);
            notifyDataSetChanged();
        }
    };
}

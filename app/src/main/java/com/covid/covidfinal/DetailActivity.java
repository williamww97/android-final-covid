package com.covid.covidfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class DetailActivity extends AppCompatActivity {
    TextView tv_country;
    TextView tv_continent;

    TextView tv_case;
    TextView tv_casetoday;

    TextView tv_death;
    TextView tv_deathtoday;

    TextView tv_recovered;
    TextView tv_recoveredtoday;

    Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent myIntent = getIntent(); // gets the previously created intent
        String countryJSON = myIntent.getStringExtra("countryDetail");
        Gson gson = new Gson();
        country = gson.fromJson(getIntent().getStringExtra("countryDetail"), Country.class);

        tv_country = findViewById(R.id.d_country);
        tv_continent = findViewById(R.id.d_continent);

        tv_case = findViewById(R.id.d_case);
        tv_casetoday = findViewById(R.id.d_case_today);

        tv_recovered = findViewById(R.id.d_recovered);
        tv_recoveredtoday = findViewById(R.id.d_recovered_today);

        tv_death = findViewById(R.id.d_death);
        tv_deathtoday = findViewById(R.id.d_death_today);

        tv_country.setText(country.getCountryName());
        tv_continent.setText(country.getContinent());

        tv_case.setText(country.getCases());
        tv_casetoday.setText(country.getTodayCases());

        tv_death.setText(country.getDeaths());
        tv_deathtoday.setText(country.getTodayDeaths());

        tv_recovered.setText(country.getRecovered());
        tv_recoveredtoday.setText(country.getTodayRecovered());
    }

    public void Bookmark(View view) {
        Bookmark bookmark = new Bookmark(
                country.getCountryName(),
                country.getContinent(),
                country.getCases(),
                country.getTodayCases(),
                country.getDeaths(),
                country.getTodayDeaths(),
                country.getRecovered(),
                country.getTodayRecovered());
        BookmarkDatabase.getDatabase(getApplicationContext()).getDao().insertAllData(bookmark);
        Toast.makeText(this,"Bookmarked!", Toast.LENGTH_SHORT).show();
    }
}
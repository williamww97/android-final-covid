package com.covid.covidfinal;

import java.util.ArrayList;

public class CountryResponse {
    private ArrayList<Country> alCountry;

    public CountryResponse(ArrayList<Country> alCountry){
        this.alCountry = alCountry;
    }

    public ArrayList<Country> getAlCountry() {
        return alCountry;
    }

    public void setAlCountry(ArrayList<Country> alCountry) {
        this.alCountry = alCountry;
    }
}

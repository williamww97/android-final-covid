package com.covid.covidfinal;

import java.util.ArrayList;

public class CaseResponse {
    private Country[] alCountry;

    public CaseResponse(Country[] alCountry){
        this.alCountry = alCountry;
    }

    public Country[] getAlCountry() {
        return alCountry;
    }

    public void setAlCountry(Country[] alCountry) {
        this.alCountry = alCountry;
    }
}

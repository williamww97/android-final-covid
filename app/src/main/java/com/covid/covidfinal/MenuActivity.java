package com.covid.covidfinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity implements CountryAdapter.CallbackAdapter {
    private SharedPreferenceConfig sharedPreferenceConfig;
    int code;
    LinearLayout linearLayout;
    RecyclerView recyclerView;
    CountryAdapter countryAdapter;
    CardView cv;
    ArrayList<Country> filteredList = new ArrayList<>();
    Country country;
    CountryResponse countryResponse;
    CaseResponse caseResponse;
    static ArrayList<Country> countries = new ArrayList<>();
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        countryAdapter = new CountryAdapter(this,this);
        countryAdapter.setDataAdapter(countryList());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        sharedPreferenceConfig = new SharedPreferenceConfig(getApplicationContext());

        cv = findViewById(R.id.country_card);
        linearLayout = findViewById(R.id.menu_activity);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //accountAdapter = new AccountAdapter(this, accountList(),this);
        recyclerView.setAdapter(countryAdapter);
    }

    public ArrayList<Country> countryList(){

/*        Country country = new Country("NICO","198623");
        countries.add(country);
        country = new Country("NICO","198623");
        countries.add(country);
        country = new Country("NICO","198623");
        countries.add(country);
        country = new Country("NICO","198623");
        countries.add(country);*/

        InterfaceAPI interfaceAPI = RetrofitInstance.getRetrofitInstance().create(InterfaceAPI.class);
        Call<Country[]> call = interfaceAPI.getListCase("https://corona.lmao.ninja/v2/countries?sort=country");
        call.enqueue(new Callback<Country[]>() {
            @Override
            public void onResponse(Call<Country[]> call, Response<Country[]> response) {
                Log.e("dab", "" + response.code());
                Log.e("dab", "" + response.body());
                code = response.code();
                Country[] a_country = response.body();
//                Log.e("dab", "" + a_country[0].getCountryName());
//                Log.e("dab", "" + a_country[0].getContinent());
                //Country[] count = gson.fromJson(response.body().toString(), Country[].class);
                //caseResponse = response.body();
                //List<Country> list = Arrays.asList(a_country);
                countries = new ArrayList<>();
                Collections.addAll(countries, a_country);

                Log.e("dab", "" + countries.get(0).getContinent());
                Log.e("dab", "" + countries.get(0).getCountryName());
            }

            @Override
            public void onFailure(Call<Country[]> call, Throwable t) {
                Log.e("tab", "failed" + t.getCause()    );
            }
        });

        Log.e("dab", "Im Done!");
        return countries;
    }

    public void Logout(View view) {
        //DROP DB
        BookmarkDatabase.getDatabase(getApplicationContext()).getDao().dropAllData();
        sharedPreferenceConfig.login_status(false);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        /*
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filteredList = new ArrayList<>();
                for(int i = 0 ; i < countries.size() ; i++){
                    if(countries.get(i).getCountryName().toLowerCase().contains(newText)){
                        filteredList.add(countries.get(i));
                    }
                }
                if(filteredList.isEmpty()){
                    showSnackBar();
                }
                countryAdapter.setDataAdapter(filteredList);
                countryAdapter.notifyDataSetChanged();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
         */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            SearchView searchView = (SearchView) item.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filteredList = new ArrayList<>();
                    for(int i = 0 ; i < countries.size() ; i++){
                        if(countries.get(i).getCountryName().toLowerCase().contains(newText)){
                            filteredList.add(countries.get(i));
                        }
                    }
                    if(filteredList.isEmpty()){
                        showSnackBar();
                    }
                    countryAdapter.setDataAdapter(filteredList);
                    countryAdapter.notifyDataSetChanged();
                    return false;
                }
            });
        }
        else if(id == R.id.action_bookmark){
            startActivity(new Intent(this, BookmarkActivity.class));
        }
        else{
            startActivity(new Intent(this, ProfileActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }



    public void showSnackBar(Country country){
        Snackbar snackbar = Snackbar.make(linearLayout ,country.getCountryName() + " " + country.getContinent(),Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    public void showSnackBar(){
        Snackbar snackbar = Snackbar.make(linearLayout ,"Account does not exist",Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    public void onClick(Country country) {
        //showSnackBar(country);
        //startActivity(new Intent(this, DetailActivity.class));
        Intent myIntent = new Intent(this, DetailActivity.class);
        Gson gson = new Gson();
        String countryJSON = gson.toJson(country);
        myIntent.putExtra("countryDetail",countryJSON);
        startActivity(myIntent);
    }
}